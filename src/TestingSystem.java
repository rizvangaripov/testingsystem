import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class TestingSystem {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        ArrayList<Question> questions = new ArrayList<>();
        buildTest(questions);
        while (true) {
            int countQuestions = startTest();
            takeATest(questions, countQuestions);
            System.out.println("Если хотите начать новый тест, то введите 1, введите 2 для добавления вопросов");
            if (scanner.nextInt() == 2) {
                scanner.nextLine();
                buildTest(questions);
            }
        }
    }

    static void buildTest(ArrayList<Question> questions) {
        boolean isNextQuestion = true;
        while (isNextQuestion) {
            System.out.println("Вопрос " + Question.getCount() + ":");
            String content = scanner.nextLine();
            System.out.println("Количество ответов: ");
            int countAnswers = scanner.nextInt();
            scanner.nextLine();
            ArrayList<String> answers = new ArrayList<>(countAnswers);
            for (int i = 0; i < countAnswers; i++) {
                System.out.println((i + 1) + " ответ");
                answers.add(i + 1 + " " + scanner.nextLine());
            }
            System.out.println("Сколько ответов правильные?");
            int countCorrectAnswers = scanner.nextInt();
            System.out.println("Под какими номерами правильные ответы?");
            ArrayList<Integer> correctAnswers = new ArrayList<>();
            for (int i = 0; i < countCorrectAnswers; i++) {
                correctAnswers.add(scanner.nextInt());
            }
            System.out.println("На сколько баллов вопрос?");
            int cost = scanner.nextInt();
            Question question = new Question(content, answers, correctAnswers, cost);
            questions.add(question);
            System.out.println("Ещё будет вопрос? (true либо false)");
            isNextQuestion = scanner.nextBoolean();
            scanner.nextLine();
        }
    }

    static int startTest() {
        System.out.println("Всего " + (Question.getCount() - 1) + " вопросов. Укажи кол-во вопросов в тестировании");
        int countQuestions = scanner.nextInt();
        while (countQuestions > Question.getCount()) {
            System.out.println("Нет столько вопросов в памяти. Всего " + Question.getCount() + " вопросов.");
            countQuestions = scanner.nextInt();
        }
        return countQuestions;
    }

    static void takeATest(ArrayList<Question> questions, int countQuestions) {
        double points = 0;
        double allPoints = 0;
        System.out.println("Напиши <готов>, если готов отвечать");
        scanner.next();
        ArrayList<Question> wrongQuestions = new ArrayList<>();
        Collections.shuffle(questions);
        long startTime = System.nanoTime();
        for (int i = 0; i < countQuestions; i++) {
            System.out.println(questions.get(i).getContent());
            for (int j = 0; j < questions.get(i).getAnswers().size(); j++) {
                System.out.println(questions.get(i).getAnswers().get(j));
            }
            System.out.println("Введи числа, под которым находится правильный ответ");
            System.out.println("На этот вопрос " + questions.get(i).getCorrectAnswers().size() +
                    " ответ(ов)");
            for (int j = 0; j < questions.get(i).getCorrectAnswers().size(); j++) {
                int userAnswer = scanner.nextInt();
                points = countPoints(points, questions, i, wrongQuestions, userAnswer);
            }
            allPoints += questions.get(i).getCost();
        }
        long estimatedTime = (System.nanoTime() - startTime) / 1000000000;
        endTest(points, allPoints, wrongQuestions, estimatedTime);
    }

    static double countPoints(double points, ArrayList<Question> questions, int i, ArrayList<Question> wrongQuestions,
                           int userAnswer) {
        if (questions.get(i).getCorrectAnswers().contains(userAnswer)) {
            points += questions.get(i).getCost() / questions.get(i).getCorrectAnswers().size();
        } else if (!wrongQuestions.contains(questions.get(i))){
            wrongQuestions.add(questions.get(i));
        }
        return points;
    }

    static void endTest(double points, double allPoints, ArrayList<Question> wrongQuestions, long estimatedTime) {
        System.out.println("Вы набрали " + points + " балл(ов) из " + allPoints + " возможных");
        for (Question wrongQuestion : wrongQuestions) {
            System.out.println("Неправильный ответ на вопрос");
            System.out.println(wrongQuestion.getContent());
            System.out.println("Правильный ответ");
            for (int j = 0; j < wrongQuestion.getCorrectAnswers().size(); j++) {
                System.out.println(wrongQuestion.getAnswers().get(wrongQuestion.getCorrectAnswers().
                        get(j) - 1));
            }
        }
        System.out.println("Время затрачено: " + estimatedTime / 60 + "мин." + estimatedTime % 60 + " с");
    }
}