import java.util.ArrayList;

public class Question {
    static private int count = 1;
    private String content;
    private ArrayList<String> answers;
    private ArrayList<Integer> correctAnswers;
    private double cost;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public ArrayList<Integer> getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(ArrayList<Integer> correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public static int getCount() {
        return count;
    }

    public Question(String content, ArrayList<String> answers, ArrayList<Integer> correctAnswers, double cost) {
        this.content = content;
        this.answers = answers;
        this.correctAnswers = correctAnswers;
        this.cost = cost;
        this.count++;
    }
}